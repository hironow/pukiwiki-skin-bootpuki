pukiwiki-skin-bootpuki
---------------------------------------------------------
Bootstrap_\ を使った\ PukiWiki_ Skin **BootPuki**

.. _Bootstrap: http://twitter.github.com/bootstrap/
.. _PukiWiki: http://pukiwiki.sourceforge.jp/

動作確認は\ ``PukiWiki 1.4.7_notb(UTF-8)``\ のみとなっています．

Demo
======

- `2column-BootPuki Ver2.0`_
- `1column-BootPuki Ver2.0`_
- `1column-BootPuki Ver1.0`_

.. _2column-BootPuki Ver2.0: http://getstarted.main.jp/demov2.0-2column/index.php?FrontPage

.. _1column-BootPuki Ver2.0: http://getstarted.main.jp/demov2.0/index.php?FrontPage

.. _1column-BootPuki Ver1.0: http://getstarted.main.jp/pukiwiki/index.php?Twitter%20Bootstrap%20Skin

特徴
========


- PC・タブレット・スマホに対応した\ **レスポンシブデザイン**
- ``MenuBar`` ``WikiMenu``\ （編集や添付，凍結など） ``パンくずリスト``\ を\ **上部Navbar**\ に表示
- ページの添付ファイルとLinkを\ **パネル**\ （Footerのツールチップ左端のiconをクリック）で開閉
- 注釈の\ **ポップアップ表示**
- `google-code-prettify`_\ によるコードハイライト

.. _google-code-prettify: https://code.google.com/p/google-code-prettify/

使い方
========

※Ver1.0の使い方は\ このWikiサイト_\ を御覧ください．

0. ``MenuBar``\ ページの編集
    Skinを導入する前に\ **上部Navbar**\ へ表示する\ ``MenuBar``\ページを編集してください．

    ``MenuBar``\ ページには

    - リンクのリスト（例. ``-[[HOME>FrontPage]]``\ ）

    だけを記入して

    ``#recent(20)``\ などはコメントアウトしてください
    ::

        - #recent(20)
        + //#recent(20)

1. Bitbucketダウンロードのタグより各Versionをダウンロード
    2column-Ver2.0：Bootstrapの\ ``-fluid``\ クラスを使用

    1column-Ver2.0：Bootstrapの\ ``-fluid``\ クラスを使用

    1column-Ver1.0：初期Version ※ダウンロードは\ こちら_\ からを推奨

2. Skinの導入
    初期設定ファイルを一部変更しています（PHPの標準出力のCSSにBootstrapのClassを追加しています）．なので，Skinファイルの置き換えだけでは導入できません．

    - ``skin``
    - ``plugin``
    - ``lib``
    - ``static``

    の4つのフォルダをPukiWikiパッケージにコピーして上書きしてください．

    ご自身のPukiWikiサイトでSkinが変更されたことを確認してください．

3. google-code-prettifyのためのPukiWiki記法
    ``pre.inc.php``\ プラグインを使っています．
    ::

        //記法
        #pre{{
        //code本文
        }}

    このプラグインの詳しい説明は作者様のサイトへどうぞ．

    `Plugin/pre.inc.php - Sonots' PukiWiki プラグイン`_

    なお，導入には\ ``pukiwiki.ini.php``\ への変更が必要です．

    56行目ぐらい
    ::

        - define('PKWKEXP_DISABLE_MULTILINE_PLUGIN_HACK', 1); // 1 = Disabled
        + define('PKWKEXP_DISABLE_MULTILINE_PLUGIN_HACK', 0); // 1 = Disabled

.. _Plugin/pre.inc.php - Sonots' PukiWiki プラグイン: http://lsx.sourceforge.jp/?Plugin%2Fpre.inc.php

Themes
========

Ver2.0では\ ``static/bootstrap/``\ を\ `Bootswatch <http://bootswatch.com/>`_\ などのTwitter Bootstrapテンプレートに置き換えることで新しいテーマに対応できます．

TO DO
================

- 2カラムデザインのSkin
- アンカーリンクで飛んだ時のズレの修正

更新履歴
================

- 2013-04-02
    2columnデザインのBootPuki Ver2.0を公開しました．

- 2013-03-31
    Skinの公開をBitbucketではじめました．これで各ファイルの差分の確認が捗ります！そして，\ Bootstrap_\ や\ `Font Awesome`_\ , `google-code-prettify <https://code.google.com/p/google-code-prettify/>`_\ のファイルを\ ``static``\ フォルダにまとめました．さらに，\ ``custom.css``\ だけに追加CSSを記述して\ ``pukiwiki.css.php``\ はコメントアウトのみとしました．

    これらの修正後，Bootstrapの\ ``-fluid``\ クラスを適用したVer2.0を公開しました．

.. _Font Awesome: http://fortawesome.github.com/Font-Awesome/

- 2012-10-01
    このWikiサイト_\ の公開とともに，BootPuki v1.0 公開．
    まだまだ完成しておらず，ひとまずの区切りということでの公開です．

    google-code-prettifyの導入を済ませました．この導入済みzipファイルは，Version 1.0として差し替えました．

.. _こちら: http://getstarted.main.jp/pukiwiki/index.php?Twitter%20Bootstrap%20Skin

.. _このWikiサイト: http://getstarted.main.jp/pukiwiki/index.php?Twitter%20Bootstrap%20Skin
