<?php
// PukiWiki - Yet another WikiWikiWeb clone.
// $Id: pukiwiki.skin.php,v 1.48 2006/03/07 14:03:02 henoheno Exp $
// Copyright (C)
//   2002-2006 PukiWiki Developers Team
//   2001-2002 Originally written by yu-ji
// License: GPL v2 or (at your option) any later version
//
// PukiWiki default skin

// ------------------------------------------------------------
// Settings (define before here, if you want)

// Set site identities
$_IMAGE['skin']['logo']     = 'pukiwiki.png';
$_IMAGE['skin']['favicon']  = 'static/ico/favicon.ico'; // Sample: 'image/favicon.ico';

// SKIN_DEFAULT_DISABLE_TOPICPATH
//   1 = Show reload URL
//   0 = Show topicpath
if (! defined('SKIN_DEFAULT_DISABLE_TOPICPATH'))
	define('SKIN_DEFAULT_DISABLE_TOPICPATH', 1); // 1, 0

// Show / Hide navigation bar UI at your choice
// NOTE: This is not stop their functionalities!
if (! defined('PKWK_SKIN_SHOW_NAVBAR'))
	define('PKWK_SKIN_SHOW_NAVBAR', 1); // 1, 0

// Show / Hide toolbar UI at your choice
// NOTE: This is not stop their functionalities!
if (! defined('PKWK_SKIN_SHOW_TOOLBAR'))
	define('PKWK_SKIN_SHOW_TOOLBAR', 1); // 1, 0

// ------------------------------------------------------------
// Code start

// Prohibit direct access
if (! defined('UI_LANG')) die('UI_LANG is not set');
if (! isset($_LANG)) die('$_LANG is not set');
if (! defined('PKWK_READONLY')) die('PKWK_READONLY is not set');

$lang  = & $_LANG['skin'];
$link  = & $_LINK;
$image = & $_IMAGE['skin'];
$rw    = ! PKWK_READONLY;

// Decide charset for CSS
$css_charset = 'iso-8859-1';
switch(UI_LANG){
	case 'ja': $css_charset = 'Shift_JIS'; break;
}

// ------------------------------------------------------------
// Output

// HTTP headers
pkwk_common_headers();
header('Cache-control: no-cache');
header('Pragma: no-cache');
header('Content-Type: text/html; charset=' . CONTENT_CHARSET);

// HTML DTD, <html>, and receive content-type
if (isset($pkwk_dtd)) {
	$meta_content_type = pkwk_output_dtd($pkwk_dtd);
} else {
	$meta_content_type = pkwk_output_dtd();
}

?>
<head>
 <?php echo $meta_content_type ?>
 <meta http-equiv="content-style-type" content="text/css" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php if ($nofollow || ! $is_read)  { ?> <meta name="robots" content="NOINDEX,NOFOLLOW" /><?php } ?>
<?php if (PKWK_ALLOW_JAVASCRIPT && isset($javascript)) { ?> <meta http-equiv="Content-Script-Type" content="text/javascript" /><?php } ?>

 <title><?php echo $title ?> - <?php echo $page_title ?></title>

 <!-- favicon and touch icons -->
 <link rel="shortcut icon" href="<?php echo $image['favicon'] ?>" />
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="static/ico/apple-touch-icon-144-precomposed.png" />
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/ico/apple-touch-icon-114-precomposed.png" />
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/ico/apple-touch-icon-72-precomposed.png" />
 <link rel="apple-touch-icon-precomposed" href="static/ico/apple-touch-icon-57-precomposed.png" />

<!-- CSS and RSS -->
<link rel="stylesheet" type="text/css" media="screen" href="skin/pukiwiki.css.php?charset=<?php echo $css_charset ?>" charset="<?php echo $css_charset ?>" />
<link rel="stylesheet" type="text/css" media="print"  href="skin/pukiwiki.css.php?charset=<?php echo $css_charset ?>&amp;media=print" charset="<?php echo $css_charset ?>" />

<link href="static/bootstrap/css/bootstrap.css" rel="stylesheet">
<style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;*/
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
</style>
<link href="static/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<link href="static/font/font-awesome.min.css" rel="stylesheet">
<!--[if IE 7]>
<link rel="stylesheet" href="static/font/font-awesome-ie7.min.css">
<![endif]-->
<link href="static/js/google-code-prettify/prettify.css" rel="stylesheet">
<link href="static/custom.css" rel="stylesheet" type="text/css">

<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php echo $link['rss'] ?>" /><?php // RSS auto-discovery ?>

<?php if (PKWK_ALLOW_JAVASCRIPT && $trackback_javascript) { ?> <script type="text/javascript" src="skin/trackback.js"></script><?php } ?>

<?php echo $head_tag ?>
 <!-- Javascript -->
<script type="text/javascript">
function addEvent(elm,listener,fn){
        try{
                elm.addEventListener(listener,fn,false);
        }catch(e){
                elm.attachEvent("on"+listener,fn);
        }
}
</script>
<script type="text/javascript" src="static/js/jquery-1.8.0.min.js"></script>
<script src="static/js/google-code-prettify/prettify.js"></script>
<script type="text/javascript" src="static/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
    $('.note_super').tooltip();

//	$('dl').addClass('dl-horizontal');

	$('div[id^="panel"]').hide();
	$('small[class^="open"]').css("cursor","pointer");
	$('i.open').addClass('icon-large');
	$('i.open').addClass('icon-folder-close-alt');
    $('i.open') . toggle(
        function() {
            $('i.open').removeClass('icon-folder-close-alt');
            $('i.open').addClass('icon-folder-open-alt');
	        $("#panel").slideToggle("fast");
           },
        function() {
            $('i.open').removeClass('icon-folder-open-alt');
            $('i.open').addClass('icon-folder-close-alt');
	        $("#panel").slideToggle("fast");
        }
    );
});
</script>
<script type="text/javascript">
addEvent(window,"load",function(){prettyPrint()});
</script>
<script type="text/javascript">
$(function(){
	$("a[href^=#]").click(function(){
		var Hash = $(this.hash);
		var HashOffset = $(Hash).offset().top;
		$("html,body").animate({
			scrollTop: HashOffset
		}, 500);
		return false;
	});
});
</script>
</head>

  <body>

<!-- Part 1: Wrap all page content here -->
<div id="wrap">

<!-- 上部固定NavBar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo $link['top'] ?>"><?php echo $page_title ?></a>
          <div class="nav-collapse collapse">

  <!-- Menu List(for MenuBar link list) -->
            <?php if (exist_plugin_convert('menu')) { ?>
              <?php echo do_plugin_convert('menu') ?>
            <?php } ?>
  <!-- END Menu List -->

<?php if(PKWK_SKIN_SHOW_NAVBAR) { ?>
<?php
function _navigator($key, $value = '', $javascript = ''){
	$lang = & $GLOBALS['_LANG']['skin'];
	$link = & $GLOBALS['_LINK'];
	if (! isset($lang[$key])) { echo 'LANG NOT FOUND'; return FALSE; }
	if (! isset($link[$key])) { echo 'LINK NOT FOUND'; return FALSE; }
	if (! PKWK_ALLOW_JAVASCRIPT) $javascript = '';

	echo '<li><a href="' . $link[$key] . '" ' . $javascript . '>' .
		(($value === '') ? $lang[$key] : $value) .
		'</a></li>';

	return TRUE;
}
?>
  <!-- Wiki menu -->
            <ul class="nav">
              <?php if ($is_page) { ?>
                <?php if ($rw) { ?>
                  <?php _navigator('edit') ?>
                  <?php if ((bool)ini_get('file_uploads')) { ?>
                    <?php _navigator('upload') ?>
                  <?php } ?>
                  <?php if ($is_read && $function_freeze) { ?>
                  <?php (! $is_freeze) ? _navigator('freeze') : _navigator('unfreeze') ?>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
              <?php _navigator('list') ?>
            </ul>
  <!-- END Wiki menu -->

  <!-- Other Wiki menu
           <ul class="nav pull-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">EDIT <b class="caret"></b></a>
                <ul class="dropdown-menu">
<?php if ($is_page) { ?>
 <?php if ($rw) { ?>
  <?php _navigator('edit') ?>
  <?php if ($is_read && $function_freeze) { ?>
    <?php (! $is_freeze) ? _navigator('freeze') : _navigator('unfreeze') ?>
  <?php } ?>
 <?php } ?>
 <?php _navigator('diff') ?>
 <?php if ($do_backup) { ?>
  <?php _navigator('backup') ?>
 <?php } ?>
 <?php if ($rw && (bool)ini_get('file_uploads')) { ?>
  <?php _navigator('upload') ?>
 <?php } ?>
<li class="divider"></li>
<?php } ?>

 <?php if ($rw) { ?>
  <?php _navigator('new') ?>
 <?php } ?>
     <?php _navigator('list') ?>
 <?php if (arg_check('list')) { ?>
   <?php _navigator('filelist') ?>
 <?php } ?>
   <?php _navigator('search') ?>
   <?php _navigator('recent') ?>
   <?php _navigator('help')   ?>


<?php if ($trackback) { ?>
<li class="divider"></li>
  <?php _navigator('trackback', $lang['trackback'] . '(' . tb_count($_page) . ')',
  ($trackback_javascript == 1) ? 'onclick="OpenTrackback(this.href); return false"' : '') ?>
<li class="divider"></li>
<?php } ?>
<?php if ($referer)   { ?>
  <?php _navigator('refer') ?>
<li class="divider"></li>
<?php } ?>
</ul>
              </li>
            </ul>
  END Other Wiki menu -->
<?php } // PKWK_SKIN_SHOW_NAVBAR ?>

            <p class="navbar-text pull-right changed-linkcolor">
              <?php if ($is_page) { ?>
              <?php require_once(PLUGIN_DIR.'topicpath.inc.php'); echo plugin_topicpath_convert(); ?>
              <?php } ?>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
<!-- END 上部固定NavBar -->

    <div class="container-fluid">

<!-- header -->
      <div class="row-fluid">
        <div class="span12 header">
          <a href="<?php echo $link['top'] ?>"><img id="logo" src="<?php echo IMAGE_DIR . $image['logo'] ?>" width="80" height="80" alt="[PukiWiki]" title="[PukiWiki]" /></a>
          <h1 class="title"><?php echo $page ?></h1>

          <?php if ($is_page) { ?>
            <?php if(SKIN_DEFAULT_DISABLE_TOPICPATH) { ?>
          <a href="<?php echo $link['reload'] ?>"><span class="small"><?php echo $link['reload'] ?></span></a>
            <?php } else { ?>
          <span class="small"><?php require_once(PLUGIN_DIR . 'topicpath.inc.php'); echo plugin_topicpath_inline(); ?></span>
            <?php } ?>
          <?php } ?>
        </div>
      </div>
<!-- END header -->

<?php echo $hr ?>

<!-- Body -->
      <?php if (arg_check('read') && exist_plugin_convert('menu')) { ?>
      <div class="row-fluid">
        <div class="span12 body"><?php echo $body ?></div>
      </div>
      <?php } else { ?>
      <div class="row-fluid">
        <div class="span12 body"><?php echo $body ?></div>
      </div>
      <?php } ?>

      <?php if ($notes != '') { ?>
      <div class="row-fluid">
        <div class="span12 note"><?php echo $notes ?></div>
      </div>
      <?php } ?>
<!-- END Body -->
    </div> <!-- /.container-fluid -->

<?php //echo $hr ?>

<div id="push"></div>
</div>

<!-- Footer -->
  <div id="footer">
    <div class="container-fluid">
    <div class="muted credit">
  <!-- Toolbar -->
    <?php if (PKWK_SKIN_SHOW_TOOLBAR) { ?>
    <div class="toolbar">

<?php
// Set toolbar-specific images
$_IMAGE['skin']['reload']   = 'reload.png';
$_IMAGE['skin']['new']      = 'new.png';
$_IMAGE['skin']['edit']     = 'edit.png';
$_IMAGE['skin']['freeze']   = 'freeze.png';
$_IMAGE['skin']['unfreeze'] = 'unfreeze.png';
$_IMAGE['skin']['diff']     = 'diff.png';
$_IMAGE['skin']['upload']   = 'file.png';
$_IMAGE['skin']['copy']     = 'copy.png';
$_IMAGE['skin']['rename']   = 'rename.png';
$_IMAGE['skin']['top']      = 'top.png';
$_IMAGE['skin']['list']     = 'list.png';
$_IMAGE['skin']['search']   = 'search.png';
$_IMAGE['skin']['recent']   = 'recentchanges.png';
$_IMAGE['skin']['backup']   = 'backup.png';
$_IMAGE['skin']['help']     = 'help.png';
$_IMAGE['skin']['rss']      = 'rss.png';
$_IMAGE['skin']['rss10']    = & $_IMAGE['skin']['rss'];
$_IMAGE['skin']['rss20']    = 'rss20.png';
$_IMAGE['skin']['rdf']      = 'rdf.png';

function _toolbar($key, $x = 20, $y = 20){
	$lang  = & $GLOBALS['_LANG']['skin'];
	$link  = & $GLOBALS['_LINK'];
	$image = & $GLOBALS['_IMAGE']['skin'];
	if (! isset($lang[$key]) ) { echo 'LANG NOT FOUND';  return FALSE; }
	if (! isset($link[$key]) ) { echo 'LINK NOT FOUND';  return FALSE; }
	if (! isset($image[$key])) { echo 'IMAGE NOT FOUND'; return FALSE; }

	echo '<a href="' . $link[$key] . '">' .
		'<img src="' . IMAGE_DIR . $image[$key] . '" width="' . $x . '" height="' . $y . '" ' .
			'alt="' . $lang[$key] . '" title="' . $lang[$key] . '" />' .
		'</a>';
	return TRUE;
}
?>

    <?php if ($is_page) { ?>
      <?php if($attaches != '' || $related != '') { ?>
  <!-- パネル用icon -->
    <i class="open icon-2x" title="このページの添付ファイルとLink"></i>
    &nbsp;
      <?php } ?>
    <?php } ?>

 <?php _toolbar('top') ?>

<?php if ($is_page) { ?>
 &nbsp;
 <?php if ($rw) { ?>
  <?php _toolbar('edit') ?>
  <?php if ($is_read && $function_freeze) { ?>
    <?php if (! $is_freeze) { _toolbar('freeze'); } else { _toolbar('unfreeze'); } ?>
  <?php } ?>
 <?php } ?>
 <?php _toolbar('diff') ?>
<?php if ($do_backup) { ?>
  <?php _toolbar('backup') ?>
<?php } ?>
<?php if ($rw) { ?>
  <?php if ((bool)ini_get('file_uploads')) { ?>
    <?php _toolbar('upload') ?>
  <?php } ?>
  <?php _toolbar('copy') ?>
  <?php _toolbar('rename') ?>
<?php } ?>
 <?php _toolbar('reload') ?>
<?php } ?>
 &nbsp;
<?php if ($rw) { ?>
  <?php _toolbar('new') ?>
<?php } ?>
 <?php _toolbar('list')   ?>
 <?php _toolbar('search') ?>
 <?php _toolbar('recent') ?>
 &nbsp; <?php _toolbar('help') ?>
 &nbsp; <?php _toolbar('rss10', 36, 14) ?>
<?php } // PKWK_SKIN_SHOW_TOOLBAR ?>

    </div>
  <!-- END Toolbar -->

  <!-- パネル -->
    <div id="panel">

  <!-- Attaches -->
    <?php if ($attaches != '') { ?>
      <div class="attach">
        <?php echo $attaches ?>
      </div>
    <?php } ?>
  <!-- END Attaches -->

  <!-- Related -->
    <?php if ($related != '') { ?>
    <div class="related">Link:
      <?php echo $related ?>
    </div>
    <?php } ?>
  <!-- END Related -->

    </div>
  <!-- END パネル -->

  <!-- Copyright -->
    <?php if ($lastmodified != '') { ?>
    <div class="lastmodified">Last-modified: <?php echo $lastmodified ?></div>
    <?php } ?>

    <div class="copyright">
      Site admin: <a href="<?php echo $modifierlink ?>"><?php echo $modifier ?></a><p />
      <?php echo S_COPYRIGHT ?>.
      Powered by PHP <?php echo PHP_VERSION ?>. HTML convert time: <?php echo $taketime ?> sec.
    </div>
  <!-- END Copyright -->
    </div>
    </div> <!-- /.container-fluid -->
  </div><!-- END Footer -->

</body>
</html>
